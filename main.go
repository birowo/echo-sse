package main

import (
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/", root)
	e.GET("/sse", sse)

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}

// Handler
func root(c echo.Context) error {
	c.Response().Header().Set("Content-Type", "text/html; charset=utf-8")
	c.Response().Write([]byte(`
<ul id="ulEl"></ul>
<script>
(new EventSource('/sse')).onmessage = evt => {
	const liEl =  document.createElement('li')
	liEl.textContent ='time: '+evt.data;
  document.getElementById('ulEl').appendChild(liEl);
};
</script>
	`))
	return nil
}
func sse(c echo.Context) error {
	c.Response().Header().Set("Cache-Control", "no-store")
	c.Response().Header().Set("Content-Type", "text/event-stream")
	for {
		c.Response().Write([]byte("data: "))
		c.Response().Write([]byte(time.Now().Format("03:04:05 PM")))
		c.Response().Write([]byte("\n\n"))
		c.Response().Flush()
		time.Sleep(time.Second)
	}
}
